import var

import telebot

#FUNC import
from func import *

API_TOKEN = var.API_MAIN

bot = telebot.TeleBot(API_TOKEN)

#Work with JSON
import json
def read():
	with open('db.json', 'r') as openfile:
		var.db = json.load(openfile)
def write():
	js = json.dumps(var.db, indent=4)
	with open("db.json", "w") as outfile:
		outfile.write(js)

#SHA256 for sign
import hashlib
#Date&Time
from datetime import date
import time



def err(message):
	bot.reply_to(message,"Что-то пошло не так, отмена операции.")
	var.steps[message.chat.id] = None

def err_reg(message):
	bot.reply_to(message,"""
Пожалуйста, авторизуйстесь.
Для этого введите свой ник (желательно игровой).""")
	var.steps[message.chat.id]="reg"

def checkauth(message):
	read()
	id=str(message.chat.id)
	if id in var.db:
		return True
	elif not id in var.db:
		err_reg(message)
		return False

def reg(message):
	read()

	id=(message.chat.id)
	nick=message.text

	try:
		getid = list(db.keys())[list(db.values()).index(nick)]
	except:
		getid=None

	if not getid in var.db:
		bot.reply_to(message,"Пожалуйста, введите пароль для аккаунта банка.")
		var.steps[message.chat.id]="pass"
		var.anick[message.chat.id]=nick
	elif getid in var.db:
		bot.reply_to(message,"""Вы пытаетесь зайти под именем другого игрока или с нового аккаунта?
Поддержка больше чем 1 аккаунта не реализована, напишите администратору - @kirill638355""")
		var.steps[message.chat.id]=None


def passs(message):
	read()
	id=str(message.chat.id)

	pas = hashlib.sha256(str.encode(message.text)).hexdigest()

	try:
		deb=var.db[var.anick[message.chat.id]+"_pas"]
	except:
		deb=None

	try:
		mon=var.db[var.anick[message.chat.id]]
	except:
		mon=None

	if pas == deb:
		bot.reply_to(message,"Поздравляю, Вы успешно авторизовались.")
		var.steps[message.chat.id]=None
		var.db[id]=var.anick[message.chat.id]
		if var.anick[message.chat.id] in var.db:
			pass
		else:
			var.db[var.anick[message.chat.id]]=0

		write()
	elif deb == None:

		var.steps[message.chat.id]=None
		var.db[id]=var.anick[message.chat.id]
		if mon == None:
			var.db[var.anick[message.chat.id]]=0


		today = date.today()
		dttm = today.strftime("%d.%m.%Y_"+time.strftime("%H:%M", time.localtime()))

		signstr=message.text+dttm
		sign=str(hashlib.sha256(str.encode(signstr)).hexdigest())

		f=open('reg.sha256','a')
		f.write(sign+"-"+dttm+"\n")
		f.close()

		bot.reply_to(message,"Поздравляю, Вы успешно зарегистрировались.")
		bot.reply_to(message,f"""
Ваш ключ восстановления пароля - {sign}
#REG
НИКОМУ НЕ ПЕРЕСЫЛАЙТЕ ЕГО, КРОМЕ - @kirill638355
ИНАЧЕ ВАШУ ЗАПИСЬ МОГУТ УКРАСТЬ.
""")
		var.db[var.anick[message.chat.id]+"_pas"]=pas

		write()
	else:
		err(message)



def unreg(message):
	read()

	print(var.db)

	id=str(message.chat.id)
	print(id)

	var.db.pop(id)
	write()

	print(var.db)

	bot.reply_to(message,"""
Вы успешно вышли из своего аккаунта.
ВНИМАНИЕ: ВАШ НИК БОЛЬШЕ НЕ ПРИВЯЗАН НИ К ОДНОМУ ТЕЛЕГРАМ АККАУНТУ.
""")

