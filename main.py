import telebot

#Keyboard
from telebot.types import ReplyKeyboardMarkup, KeyboardButton

#0.00000000009 output
from numpy import format_float_positional

#VAR import
import var
var.init()

#FUNC import
from func import *

API_TOKEN = var.API_MAIN

bot = telebot.TeleBot(API_TOKEN)


@bot.message_handler(commands=['start'])
def welcome(message):
	markup = ReplyKeyboardMarkup(resize_keyboard=True)

	markup.add('Баланс')
	markup.add('Ввести/Вывести')
	markup.add('Перевод')
	markup.add('Помощь')

	bot.reply_to(message, """
Используя данный банк, вы соглашаетесь с следующими положениями в ссылке - https://t.me/cryptodm_bank/8
""",reply_markup=markup)


@bot.message_handler(commands=['help'])
def help(message):
	bot.reply_to(message, """
┌─────────Помощь─────────
├[Основная информация](https://t.me/cryptodm_bank/5)
├[Спец.команды](https://t.me/cryptodm_bank/4)
├[Соглашения](https://t.me/cryptodm_bank/8)
│
├Разработчик/Администратор
├@kirill638355
├Ник: \_SAN5\_SkeLet0n\_
│
├[Исходный код](https://gitlab.com/justuser31/dm_moneybot)
└─────────────────────────
""",parse_mode='Markdown')



@bot.message_handler(commands=['unreg'])
def unregs(message):
        unreg(message)

@bot.message_handler(commands=['passwd'])
def passwd(message):
	if checkauth(message):
		bot.reply_to(message, "Введите старый пароль ➣")
		var.steps[message.chat.id]="passwd"

@bot.message_handler(commands=['balance'])
def balance(message):
	read()

	if checkauth(message):
		name=var.db[str(message.chat.id)]
		sum = format_float_positional(var.db[name])
		bot.reply_to(message,f"""
ID/Имя ➣ {var.db[str(message.chat.id)]}
Баланс ➣ {sum} CDM
""")


@bot.message_handler(commands=['change'])
def change(message):
	if checkauth(message):
		var.steps[message.chat.id]="change"
		bot.reply_to(message,"""
1 CDM = 1 алмаз

┌─────Расписание──
│17:00 - 22:00 (По Москве)
├────────────────
└➣Введите время или напишите "отмена"
""")
	else:
		err(message)


@bot.message_handler(commands=['pay'])
def pay(message):
	if checkauth(message):
		bot.reply_to(message,"Сумма ➣")
		var.steps[message.chat.id]="pay_num"


@bot.message_handler(func=lambda message: True)
def checks(message):

	if message.text == 'Баланс':
		balance(message)
	elif message.text == 'Ввести/Вывести':
		change(message)
	elif message.text == 'Перевод':
		pay(message)
	elif message.text == 'Помощь':
		help(message)
	else:
		try:
			print(var.steps[message.chat.id])
		except:
			var.steps[message.chat.id] = None



		if var.steps[message.chat.id] == "reg":
			reg(message)

		elif var.steps[message.chat.id] == "pass":
			passs(message)

		elif var.steps[message.chat.id] == "passwd":
			read()

			oldpas = var.db[var.db[str(message.chat.id)]+"_pas"]
			enteredpas = hashlib.sha256(str.encode(message.text)).hexdigest()

			if str(oldpas) == str(enteredpas):
				bot.reply_to(message, "Введите новый пароль ➣")
				var.steps[message.chat.id] = "newpass"
			else:
				bot.reply_to(message, "Неверный пароль.")
				var.steps[message.chat.id] = None

		elif var.steps[message.chat.id] == "newpass":
			newpass = hashlib.sha256(str.encode(message.text)).hexdigest()

			var.db[var.db[str(message.chat.id)]+"_pas"] = newpass

			write()
			bot.reply_to(message, "Пароль успешено изменён.")
			var.steps[message.chat.id] = None


		elif var.steps[message.chat.id] == "change":
			if (message.text).lower() != "отмена":
				try:
					read()
					bot.send_message(2057834471,"#######################\nИгрок "+str(var.db[str(message.chat.id)])+" (@"+message.from_user.username+")"+" просит встречу в "+message.text+"\n#######################\n")
					bot.reply_to(message,"Запрос успешно отправлен!")
					var.steps[message.chat.id]=None
				except:
					err(message)
			elif (message.text).lower() == "отмена":
				bot.reply_to(message,"Операция отменена.")
				var.steps[message.chat.id]=None


		elif var.steps[message.chat.id] == "pay_num":
			try:
#			if True:
				read()
				#FLOAT
				var.pnum[message.chat.id] = float(message.text)
				if var.pnum[message.chat.id] > 0.00000000001:
					if float(var.db[var.db[str(message.chat.id)]]) >= var.pnum[message.chat.id]:
						bot.reply_to(message,"Ник адресата ➣")
						var.steps[message.chat.id]="pnick"
					else:
						err(message)
				else:
					print(1)
					err(message)
			except:
				err(message)

		elif var.steps[message.chat.id] == "pnick":
			if message.text == "sans":
				var.pnick[message.chat.id] = "_SAN5_SkeLet0n_"
			else:
				var.pnick[message.chat.id] = message.text

			if var.pnum[message.chat.id] < 1:
				sum = format_float_positional(var.pnum[message.chat.id])
			else:
				sum = str(var.pnum[message.chat.id])

			bot.reply_to(message,f"""
Ник ➣ {var.pnick[message.chat.id]}
Сумма ➣ {sum}
Для подтверждения напишите 'да' """)
			var.steps[message.chat.id] = "pay_valid"

		elif var.steps[message.chat.id] == "pay_valid":
			if message.text == 'да':
				try:
#				if 1 == 1:
					var.db[var.pnick[message.chat.id]] = var.db[var.pnick[message.chat.id]]+var.pnum[message.chat.id]
					var.db[var.db[str(message.chat.id)]] = var.db[var.db[str(message.chat.id)]]-var.pnum[message.chat.id]

					#Date
					today = date.today()

					payer=var.db[str(message.chat.id)]
#					payer_bill=str(message.chat.id)
#					sum=str(var.pnum[message.chat.id])


					if var.pnum[message.chat.id] < 1:
						sum = format_float_positional(var.pnum[message.chat.id])
					else:
						sum = str(var.pnum[message.chat.id])

					dttm=today.strftime("%d.%m.%Y_"+time.strftime("%H:%M", time.localtime()))

					signstr=payer+var.pnick[message.chat.id]+dttm
					sign=str(hashlib.sha1(str.encode(signstr)).hexdigest())

					bot.reply_to(message,"Успешно")
					bot.reply_to(message,f"""
-----ЧЕК----
Плательщик: {payer}
Сумма: {sum} CDM
~~~~~~~~~~
Получатель: {var.pnick[message.chat.id]}
Время&Дата: {dttm}
------------
~CryptoDM~
ПОДПИСЬ: {sign}
___________ 
""")
					f=open('signs.sha256','a')
					f.write(sign+"-"+dttm+"\n")
					f.close()

					try:
						getid = list(var.db.keys())[list(var.db.values()).index(var.pnick[message.chat.id])]
						bot.send_message(getid,f"""
╔════════════════════════
║Вам перевели ➣ {sum} CDM
╚════════════════════════
""")
					except:
						pass
					write()
				except:
					err(message)
			else:
				var.steps[message.chat.id] = None
				bot.reply_to(message,"Вы отменили операцию")

bot.infinity_polling()
