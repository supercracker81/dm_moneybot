import telebot

import var
var.init()

TOKEN = var.API_MAIN
bot = telebot.TeleBot(TOKEN)

#JSON for database
import json
db = {"Nick":10}
def read():
        #Read db and push
        global db
        with open('db.json', 'r') as openfile:
                db = json.load(openfile)

read()

message = """
Технические работы завершены, но система запущена в тестовом режиме.
При нахождении уязвимостей, пожалуйста, сообщите их владельцу банка, награду дам.

Из изменений:
- В соглашения добавлен пункт "Уязвимости в системе"
- Подменить сумму или ник перевода уже невозможно
- Все счета переведены на новый формат

Курс как и раньше - 1 алмаз - 1 CDM
"""

for i in db:
	if True:
		try:
			id=int(i)
		except:
			id=None

		if id != None:
			try:
#				bot.send_message(id,message,parse_mode='Markdown')
				bot.send_message(id,message)
				print("Pass ", id)
			except:
				print("Error ",id)
